// 1 App == 1Module 
(function(){
    var ilearn = angular.module("ilearn", ["ngRoute"]).config(function($routeProvider){
        $routeProvider
        .when("/login", {
            templateUrl:"/views/login.html",
            controller:"loginCntrl"
        })
        .when("/courses", {
            templateUrl:"/views/courses.html",
            controller:"coursesCntrl"
        })
        .when("/contacts", {
            templateUrl:"/views/contacts.html",
            controller:"contactsCntrl"
        })
        .otherwise({redirectTo:"/login"})
    })
})() //IIFE - immediately invoked function expression
