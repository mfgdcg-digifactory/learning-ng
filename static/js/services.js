(function(){
    angular.module("ilearn").service("apiSrv", function($http,$q, $timeout){
        this.authenticate_user = function(email,auth){
            var deferred = $q.defer();
            var url = "http://127.0.0.1:4000/api/users/"+email+"?auth="+auth;
            // http://127.0.0.1:4000/users/kneerunjun@gmail.com?auth=45453453
            console.log(url);
            $http.get(url).then(function(response){
                // success function
                console.log(response.data);
                console.log(response.status);
                console.log(response.statusText);
                deferred.resolve(response.data);
            }, function(response){
                console.log(response.status);
                console.log(response.statusText);
                deferred.reject(response.status);
            })
            // $timeout(function(){
            //     deferred.resolve({
            //         authenticated:true,
            //         cached:false,
            //         isadmin:true
            //     })
            // },3000)
            return deferred.promise;
        }
    })
})()