(function(){
    angular.module("ilearn").controller("loginCntrl", function($scope, apiSrv, $location){
        $scope.validationErr = false;
        $scope.user={
            email:"",
            password:""
        }
        $scope.login = function(){
            if($scope.user.email== "" || $scope.user.password== ""){
                $scope.validationErr= true
            }
            if($scope.validationErr == false){
                apiSrv.authenticate_user($scope.user.email,$scope.user.password).then(function(data){
                    console.log(data);
                    $location.url("/courses")
                }, function(data){
                    console.log(data);
                })
            }
            else{
                console.log("Check one or more of your inputs");
            }
           
        }
    })
    .controller("coursesCntrl", function($scope){
        $scope.searchPhrase = "";
        $scope.courses = [
            {id:"dfsfsdf",title:"Go Lang Jumpstart",mode:"WebEx",speaker:"niranjan_awati",duration:10,level:101},
            {id:"fsdfdsf",title:"JS Jumpstart",mode:"WebEx",speaker:"niranjan_awati",duration:10,level:101},
            {id:"fsdfsdf",title:"Angular JS1.xx",mode:"WebEx",speaker:"niranjan_awati",duration:10,level:101},
            {id:"345edf",title:"Django",mode:"WebEx",speaker:"niranjan_awati",duration:10,level:101},
            {id:"tret565",title:"Python",mode:"WebEx",speaker:"niranjan_awati",duration:10,level:101},
            {id:"355435v",title:"IoT basics",mode:"WebEx",speaker:"niranjan_awati",duration:10,level:101},
        ]
    })
    .filter("likely",function(){
        return function(coll,phrase){
            var result = coll
            if(phrase!=""){
                result  = [];
                coll.forEach(function(el, index){
                    if(el.title.toLowerCase().indexOf(phrase.toLowerCase())>-1){
                        result.push(el)
                    }
                })
            }
            return result
        }
    })
    .controller("contactsCntrl",function($scope){
        $scope.user = {
            name:"niranjan",
            lastname:"awati",
            email:"niranjan_awati",
            id:41993,
            location:"pune"
        }
        $scope.user_submit = function(){
            console.log("User is about to submit changes");
        }
    })
})()