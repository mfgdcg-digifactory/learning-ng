(function(){
    // camelCase
    // snake-case, kebab case
    // train_case
    // RegularCase - MS
    // definition of the directive
    angular.module("ilearn").directive("contactCard", function(){
        return {
            restrict : "E",
            replace : false, 
            scope :{
                first:"=",
                last:"=",
                atstart:"@",
                bodyClicked:"&"
            }, // isolated scope
            templateUrl:"/views/contactcard.html",
            controller: function($scope){
                $scope.bodyVisible = $scope.atstart=="collapse"?false:true;
                $scope.header_clicked  = function(){
                    $scope.bodyVisible = !$scope.bodyVisible;
                }
            }
        }
    })
})()