package main

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	fmt.Println("We are now starting the gin web server")
	r := gin.Default()
	r.LoadHTMLGlob("templates/*")
	r.Static("/libs", "./node_modules")
	r.Static("/locallibs", "./static/js")
	r.Static("/views", "./static/views")

	r.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.html", nil)
	})
	api := r.Group("/api")
	api.GET("/courses", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"courses": nil,
		})
	})
	api.GET("/users/:email", func(c *gin.Context) {
		fmt.Println(c.Param("email"))
		fmt.Println(c.Query("auth"))
		c.JSON(http.StatusOK, gin.H{
			"authenticated": true,
			"cached":        false,
			"isadmin":       true,
		})
		return
	})
	fmt.Println("http://127.0.0.1:4000/")
	r.Run(":4000")
}
